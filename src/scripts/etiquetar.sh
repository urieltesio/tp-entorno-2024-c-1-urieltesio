#!/bin/bash

if ls "$IMAGESDIR"/*.jpg 1> /dev/null 2>&1; then
  for IMAGE in "$IMAGESDIR"/*.jpg; do
    BASE_NAME="${IMAGE##*/}"
    BASE_NAME="${BASE_NAME%.*}"
    
    TAG_FILE="$IMAGESDIR/$BASE_NAME.tag"
    
    output=$(yolo predict model=yolov8n.pt source="$IMAGE")
    labels=$(echo "$output" | grep -Eo '[0-9]+ [a-zA-Z]+')
    
    echo "$labels" > "$TAG_FILE"
    
    if [ -s "$TAG_FILE" ]; then
      echo "Etiquetas guardadas en $TAG_FILE"
    else
      echo "No se obtuvieron etiquetas para $IMAGE"
      rm "$TAG_FILE"
    fi
  done
else
  echo "No se encontraron archivos .jpg en $IMAGESDIR."
fi

for TAG_FILE in "$IMAGESDIR"/*.tag; do
  BASE_NAME="${TAG_FILE##*/}"
  BASE_NAME="${BASE_NAME%.*}"
  
  CONTENT=$(cat "$TAG_FILE")
  
  {
    echo "$BASE_NAME.tag:"
    if [ -z "$CONTENT" ]; then
      echo "Sin etiquetas detectadas"
    else
      echo "$CONTENT"
    fi
  } > "$TAG_FILE.tmp" && mv "$TAG_FILE.tmp" "$TAG_FILE"
  awk 'NR==1 || NR>6 {print}' "$TAG_FILE" > "$TAG_FILE.tmp" && mv "$TAG_FILE.tmp" "$TAG_FILE"
done


echo "Etiquetas procesadas y guardadas en los archivos .tag correspondientes."

