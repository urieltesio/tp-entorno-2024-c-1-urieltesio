#!/bin/bash

DESTINO="../../imagenes"

ARCHIVO_COMPRESION="contenido_comprimido.tar.gz"

cd "$DESTINO" || { echo "No se puede acceder a la carpeta $DESTINO"; exit 1; }

tar -czf "$ARCHIVO_COMPRESION" *

sha256sum "$ARCHIVO_COMPRESION" > "$ARCHIVO_COMPRESION.sha256"

echo "El archivo ha sido comprimido y su suma de verificación ha sido generada en $DESTINO"
