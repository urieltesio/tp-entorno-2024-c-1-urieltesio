#!/bin/bash

echo "Ingrese la etiqueta a eliminar: "
read ETIQUETA

if [ -z "$ETIQUETA" ]; then
  echo "No se ingresó ninguna etiqueta. No se eliminarán imágenes."
  exit 0
fi

cd "$IMAGESDIR" || { echo "No se pudo cambiar al directorio $IMAGESDIR"; exit 1; }

CONTIMG=0

for TAG_FILE in *.tag; do
  if [ -s "$TAG_FILE" ]; then
    SEGUNDA_LINEA=$(sed -n '2p' "$TAG_FILE")
    if [[ "$SEGUNDA_LINEA" == *"$ETIQUETA"* ]]; then
      BASE_NAME="${TAG_FILE%.tag}"
      rm -f "$TAG_FILE" "$BASE_NAME.jpg"
      
      if [ $? -eq 0 ]; then
        echo "Eliminado: $TAG_FILE y $BASE_NAME.jpg"
      else
        echo "Error al eliminar: $TAG_FILE y $BASE_NAME.jpg"
      fi
      
      CONTIMG=1
    fi
  fi
done

if [ $CONTIMG -eq 0 ]; then
  echo "No se encontró ninguna imagen con la etiqueta '$ETIQUETA'."
else
  echo "Proceso completado."
fi
