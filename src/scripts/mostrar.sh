#!/bin/sh

echo "Ingrese la etiqueta que desea buscar:"
read ETIQUETA

if [ -z "$ETIQUETA" ]; then
    echo "No se ha ingresado ninguna etiqueta. Saliendo..."
    exit 1
fi

for TAG_FILE in "$IMAGESDIR"/*.tag; do
    # Busca la etiqueta a partir de la segunda línea en adelante
    if sed -n '2,$p' "$TAG_FILE" | grep -q "$ETIQUETA"; then
        BASE_NAME="${TAG_FILE##*/}"
        BASE_NAME="${BASE_NAME%.*}"

        echo "Etiqueta '$ETIQUETA' encontrada en la imagen: $BASE_NAME"

        # Generar arte ASCII de la imagen
        echo "\nArte ASCII de la imagen:"
        jp2a "$IMAGESDIR/$BASE_NAME.jpg"  # Asegúrate de tener jp2a instalado
    fi
done

