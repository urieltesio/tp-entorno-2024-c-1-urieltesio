#!/bin/bash

DEST_DIR=$(readlink -f $SOURCEDIR/../imagenes)

BASE_URL="https://loremflickr.com/512/512"

if [ $# -eq 1 ]; then
  IMAGE_URL="$BASE_URL/$1"
else
  IMAGE_URL="$BASE_URL"
fi

UNIQUE_URL="$IMAGE_URL?random=$(date +%s%N)"

echo "Descargando imagen desde: $UNIQUE_URL"

TEMP_FILE="$DEST_DIR/temp_image_$(date +%s%N).jpg"
curl -s -L -o "$TEMP_FILE" "$UNIQUE_URL"

if [ $? -ne 0 ]; then
  echo "Error: No se pudo descargar la imagen."
else
  echo "Imagen descargada y guardada como $TEMP_FILE"
fi

