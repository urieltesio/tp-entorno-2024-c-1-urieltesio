# syntax = edrevo/dockerfile-plus
INCLUDE+ .Dockerfile.base

# Instalar los programas necesarios
RUN apt-get update && \
    apt-get install -y curl && \
    rm -rf /var/lib/apt/lists/*

# Para distribuciones basadas en Debian (Ubuntu, etc.)
RUN apt-get update && apt-get install -y jp2a


# Configuración de la aplicación
ENV TERM=xterm
ENV COLORTERM=24bit

# Copiar la configuración y los scripts
COPY src/ /app/

# Dar permisos de ejecución a los scripts
RUN chmod +x /app/scripts/*.sh /app/menu/*.sh /app/main.sh

WORKDIR /app

# Establecer el script principal como comando por defecto
ENTRYPOINT ["/app/main.sh"]


